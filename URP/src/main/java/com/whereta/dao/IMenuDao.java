package com.whereta.dao;

import com.whereta.model.Menu;

import java.util.List;

/**
 * Created by vincent on 15-8-27.
 */
public interface IMenuDao {

    List<Menu> selectAll();

    Menu get(List<Menu> menus, int id);

    int createMenu(Menu menu);

    int deleteMenu(int id);

    int updateMenu(Menu menu);

}
