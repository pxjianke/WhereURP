package com.whereta.dao;

import com.github.pagehelper.PageInfo;
import com.whereta.model.LoginLog;

/**
 * Created by vincent on 15-9-12.
 */
public interface ILoginLogDao {

    PageInfo<LoginLog> query(int pageNow,int count);

}
