package com.whereta.mapper;

import com.whereta.model.MessageBoard;
import com.whereta.model.MessageBoardWithBLOBs;
import org.springframework.stereotype.Component;

@Component("messageBoardMapper")
public interface MessageBoardMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(MessageBoardWithBLOBs record);

    int insertSelective(MessageBoardWithBLOBs record);

    MessageBoardWithBLOBs selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MessageBoardWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(MessageBoardWithBLOBs record);

    int updateByPrimaryKey(MessageBoard record);
}